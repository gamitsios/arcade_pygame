import pygame
from pygame import mixer
import random
import math

# Initialize the pygame
pygame.init()

# Declaration of the consts that are used throughout the game
# It'll help making the code more readable
WIDTH = 1000
HEIGHT = 800
GREEN = (0, 100, 0)
BLUE = (0, 96, 255)
BLACK = (0, 0, 0)
SIZE_OF_ENEMIES = 10
PROBABILITY_HELICOPTER = 0.001
PROBABILITY_PLANE = 0.0005

# Create the screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))

# Set title and icon
pygame.display.set_caption("My River Raid")
icon = pygame.image.load("spaceship_logo.png")
pygame.display.set_icon(icon)

# Player variables
playerImg = pygame.image.load("player.png")
playerX = 440
playerY = 600
player_speed = 0.8
playerX_change = 0
player_is_dead = False
player_lives = 3

# Tree variables
treeImg = pygame.image.load('cherry_tree.png')
tree_pos = [random.randint(0, 134), 0]  # The X and Y coordinates of the tree
tree_speed = 0.7
tree_list = [tree_pos]

# Enemy1 variables
enemy_pos = [random.randint(200, WIDTH - 264), 0]  # The X and Y coordinates of the enemy
enemy_image = pygame.image.load("enemy_helicopter.png")
enemy_speed = 0.7
enemy_list = [enemy_pos]

# Enemy2 variables
enemy_plane_pos = [WIDTH, random.randint(0, 350)]  # The X and Y coordinates of the enemy
enemy_plane_image = pygame.image.load("plane.png")
enemy_plane_speed = 0.7
enemy_plane_list = [enemy_plane_pos]
# Enemy2 left to right variables
enemy_plane_ltr_pos = [0, random.randint(0, 350)]  # The X and Y coordinates of the enemy
enemy_plane_ltr_image = pygame.image.load("plane_ltr.png")
enemy_plane_ltr_speed = 0.7
enemy_plane_ltr_list = [enemy_plane_ltr_pos]

# Bullet variables
bulletImg = pygame.image.load("bullet.png")
bulletX = 0
bulletY = 600
bulletX_change = 0
bulletY_change = 9
bullet_state = "ready"

# Score text
score_value = 0
highscore_value = 0
font = pygame.font.Font('freesansbold.ttf', 32)
textX = 10
textY = 10

# Game Over text
over_font = pygame.font.Font('freesansbold.ttf', 64)
restart_font = pygame.font.Font('freesansbold.ttf', 20)
highscore_font = pygame.font.Font('freesansbold.ttf', 40)


# Functions to create, recall, draw and move the trees:
def drop_trees(tree_list):
    probability = random.random()
    probability2 = random.random()
    if len(tree_list) < 10 and probability < 0.003:
        if probability2 < 0.5:
            x_pos = random.randint(0, 134)
        else:
            x_pos = random.randint(832, 934)
        y_pos = 0
        tree_list.append([x_pos, y_pos])


def recall_trees(tree_list):
    for tree_pos in tree_list:
        tree_pos[0] = 2000
        tree_pos[1] = 2000


def draw_trees(tree_list):
    for tree_pos in tree_list:
        screen.blit(treeImg, (tree_pos[0], tree_pos[1]))


def update_tree_positions(tree_list):
    for idx, tree_pos in enumerate(tree_list):

        if tree_pos[1] >= 0 and tree_pos[1] < HEIGHT:
            tree_pos[1] += tree_speed
        else:
            tree_list.pop(idx)


#  Functions to create, recall, draw and move the enemies:
def drop_enemies(enemy_list):
    delay = random.random()
    if len(enemy_list) < SIZE_OF_ENEMIES and delay < PROBABILITY_HELICOPTER:
        x_pos = random.randint(200, WIDTH - 264)
        y_pos = 0
        enemy_list.append([x_pos, y_pos])


def recall_enemies(enemy_list):
    for enemy_pos in enemy_list:
        enemy_pos[0] = 2000
        enemy_pos[1] = 2000


def draw_enemies(enemy_list):
    for enemy_pos in enemy_list:
        screen.blit(enemy_image, (enemy_pos[0], enemy_pos[1]))


def update_enemy_positions(enemy_list):
    for idx, enemy_pos in enumerate(enemy_list):

        if enemy_pos[1] >= 0 and enemy_pos[1] < HEIGHT:
            enemy_pos[1] += enemy_speed
        else:
            enemy_list.pop(idx)  # if an enemy comes out of bounds, pop it and let the drop_enemies function to take
            #  care of it


#  Check if the player collides with enemy
def collision_check(enemy_list):
    global playerX, playerY
    for enemy_pos in enemy_list:
        if isCollision(enemy_pos[0], enemy_pos[1], playerX, playerY, 57):
            return True
    return False


# Check if the bullet collides with enemy
def collision_check_for_bullet(enemy_list):
    global bulletX, bulletY, bullet_state
    for idx, enemy_pos in enumerate(enemy_list):
        if bullet_state == "fire":
            if isCollision(enemy_pos[0], enemy_pos[1], bulletX, bulletY, 27):
                enemy_list.pop(idx)  # if an enemy collides with bullet , pop it and let the drop_enemies function to
                #  take care of it
                return True
    return False


#  Functions to create, recall, draw and move the planes:
def drop_plane_enemies(enemy_plane_list):
    delay = random.random()
    prob = random.random()
    if len(enemy_plane_list) < SIZE_OF_ENEMIES and delay < PROBABILITY_PLANE:
        x_pos = WIDTH
        y_pos = random.randint(0, 350)
        enemy_plane_list.append([x_pos, y_pos])


def recall_plane_enemies(enemy_plane_list):
    for enemy_plane_pos in enemy_plane_list:
        enemy_plane_pos[0] = 2000
        enemy_plane_pos[1] = 2000


def draw_plane_enemies(enemy_plane_list):
    for enemy_plane_pos in enemy_plane_list:
        screen.blit(enemy_plane_image, (enemy_plane_pos[0], enemy_plane_pos[1]))


def update_enemy_plane_positions(enemy_plane_list):
    for idx, enemy_plane_pos in enumerate(enemy_plane_list):

        if enemy_plane_pos[0] > 0 and enemy_plane_pos[1] < HEIGHT:
            enemy_plane_pos[0] -= enemy_plane_speed
            enemy_plane_pos[1] += enemy_plane_speed
        else:
            enemy_plane_list.pop(idx)  # if an enemy comes out of bounds, pop it and let the drop_plane_enemies
            # function to take care of it


#  Check if the player collides with enemy plane
def collision_plane_check(enemy_plane_list):
    global playerX, playerY
    for enemy_plane_pos in enemy_plane_list:
        if isCollision(enemy_plane_pos[0], enemy_plane_pos[1], playerX, playerY, 57):
            return True
    return False


# Check if the bullet collides with enemy plane
def collision_plane_check_for_bullet(enemy_plane_list):
    global bulletX, bulletY, bullet_state
    for idx, enemy_plane_pos in enumerate(enemy_plane_list):
        if bullet_state == "fire":
            if isCollision(enemy_plane_pos[0], enemy_plane_pos[1], bulletX, bulletY, 27):
                enemy_plane_list.pop(idx)  # if an enemy collides with bullet , pop it and let the drop_plane_enemies
                # function to take care of it
                return True
    return False


# Functions to create, draw move and recall the planes that are coming from left to right:
def drop_plane_ltr_enemies(enemy_plane_ltr_list):
    delay = random.random()
    if len(enemy_plane_ltr_list) < SIZE_OF_ENEMIES and delay < PROBABILITY_PLANE:
        x_pos = 0
        y_pos = random.randint(0, 350)
        enemy_plane_ltr_list.append([x_pos, y_pos])


def recall_plane_ltr_enemies(enemy_plane_ltr_list):
    for enemy_plane_ltr_pos in enemy_plane_ltr_list:
        enemy_plane_ltr_pos[0] = 2000
        enemy_plane_ltr_pos[1] = 2000


def draw_plane_ltr_enemies(enemy_plane_ltr_list):
    for enemy_plane_ltr_pos in enemy_plane_ltr_list:
        screen.blit(enemy_plane_ltr_image, (enemy_plane_ltr_pos[0], enemy_plane_ltr_pos[1]))


def update_enemy_plane_ltr_positions(enemy_plane_ltr_list):
    for idx, enemy_plane_ltr_pos in enumerate(enemy_plane_ltr_list):

        if enemy_plane_ltr_pos[0] <= WIDTH and enemy_plane_ltr_pos[1] < HEIGHT:
            enemy_plane_ltr_pos[0] += enemy_plane_ltr_speed
            enemy_plane_ltr_pos[1] += enemy_plane_ltr_speed
        else:
            enemy_plane_ltr_list.pop(idx)


#  Check if the player collides with enemy
def collision_plane_ltr_check(enemy_plane_ltr_list):
    global playerX, playerY
    for enemy_plane_ltr_pos in enemy_plane_ltr_list:
        if isCollision(enemy_plane_ltr_pos[0], enemy_plane_ltr_pos[1], playerX, playerY, 57):
            return True
    return False


# Check if the bullet collides with enemy
def collision_plane_ltr_check_for_bullet(enemy_plane_ltr_list):
    global bulletX, bulletY, bullet_state
    for idx, enemy_plane_ltr_pos in enumerate(enemy_plane_ltr_list):
        if bullet_state == "fire":
            if isCollision(enemy_plane_ltr_pos[0], enemy_plane_ltr_pos[1], bulletX, bulletY, 27):
                enemy_plane_ltr_list.pop(idx)
                return True
    return False


def show_score(x, y):
    score = font.render("Score: " + str(score_value), True, BLACK)
    remaining_lives = restart_font.render("Remaining lives:" + str(player_lives), True, BLACK)
    screen.blit(score, (x, y))
    screen.blit(remaining_lives, (x, y + 50))


# Read and write the high score to txt so if the user closes the app the data are not lost
def highscore(highscore_value):
    file_object = open("scores.txt", "r")

    old = file_object.read()
    olde = int(old)
    if highscore_value > olde:
        new = str(highscore_value)
        file = open("scores.txt", "w+")
        file.write(new)
        file_object.close()
        file.close()

    else:
        file_object.close()


# As the player progresses the game becomes more challenging
def level_up():
    global score_value, enemy_speed, enemy_plane_speed, enemy_plane_ltr_speed, tree_speed, player_speed, SIZE_OF_ENEMIES, PROBABILITY_HELICOPTER, PROBABILITY_PLANE
    if score_value < 5:
        player_speed = 1.7
        enemy_speed = 1.8
        enemy_plane_speed = 1.6
        enemy_plane_ltr_speed = 1.6
        tree_speed = 1.8
        SIZE_OF_ENEMIES = 10
        PROBABILITY_PLANE = 0.001
        PROBABILITY_HELICOPTER = 0.0025
    elif score_value < 10:
        player_speed = 2
        enemy_speed = 2.1
        enemy_plane_speed = 1.6
        enemy_plane_ltr_speed = 1.6
        tree_speed = 2.1
        SIZE_OF_ENEMIES = 20
        PROBABILITY_PLANE = 0.001
        PROBABILITY_HELICOPTER = 0.0025
    elif score_value < 15:
        player_speed = 2.4
        enemy_speed = 2.4
        enemy_plane_speed = 1.8
        enemy_plane_ltr_speed = 1.8
        tree_speed = 2.4
        SIZE_OF_ENEMIES = 30
        PROBABILITY_PLANE = 0.001
        PROBABILITY_HELICOPTER = 0.003
    elif score_value < 20:
        player_speed = 2.6
        enemy_speed = 2.9
        enemy_plane_speed = 2.3
        enemy_plane_ltr_speed = 2.3
        tree_speed = 2.9
        SIZE_OF_ENEMIES = 40
        PROBABILITY_PLANE = 0.0015
        PROBABILITY_HELICOPTER = 0.004
    else:
        player_speed = 2.9
        enemy_speed = 3.5
        enemy_plane_speed = 2.6
        enemy_plane_ltr_speed = 2.6
        tree_speed = 3.5
        SIZE_OF_ENEMIES = 40
        PROBABILITY_PLANE = 0.002
        PROBABILITY_HELICOPTER = 0.005


# this function called when the player dies, resets all the variables to the initial values
# and the game over screen appears
def game_over():
    global playerX, playerY, playerX_change, player_is_dead, \
        score_value, bulletX, bulletY, bulletX_change, bulletY_change, bullet_state, highscore_value, player_lives

    playerX = 440
    playerY = 600
    playerX_change = 0
    player_lives = 3
    bulletX = 0
    bulletY = 600
    bulletX_change = 0
    bulletY_change = 9
    bullet_state = "ready"
    # recall all the objects showing in the screen
    recall_enemies(enemy_list)
    recall_plane_enemies(enemy_plane_list)
    recall_plane_ltr_enemies(enemy_plane_ltr_list)
    recall_trees(tree_list)
    over_text = over_font.render("GAME OVER", True, BLACK)
    score_text = font.render("Your score was: " + str(score_value), True, BLACK)
    restart_text = restart_font.render("Press enter to start gain", True, BLACK)
    highscore_text = highscore_font.render("High score: " + str(highscore_value), True, BLACK)
    screen.fill(GREEN)
    screen.blit(over_text, (300, 350))
    screen.blit(score_text, (350, 450))
    screen.blit(restart_text, (370, 500))
    screen.blit(highscore_text, (23, 35))


def game_reset():
    global playerX, playerY, playerX_change, player_is_dead, \
        score_value, bulletX, bulletY, bulletX_change, bulletY_change, bullet_state, highscore_value, player_lives
    playerX = 440
    playerY = 600
    playerX_change = 0
    bulletX = 0
    bulletY = 600
    bulletX_change = 0
    bulletY_change = 9
    bullet_state = "ready"
    # recall all the objects showing in the screen
    recall_enemies(enemy_list)
    recall_plane_enemies(enemy_plane_list)
    recall_plane_ltr_enemies(enemy_plane_ltr_list)
    recall_trees(tree_list)
    player_lives -= 1
    print (player_lives)


def player(x, y):
    screen.blit(playerImg, (x, y))


def fire_bullet(x, y):
    global bullet_state
    bullet_state = "fire"
    screen.blit(bulletImg, (x + 16, y + 10))


# Checks the distance of the 2 objects we pass and if its smaller than the given distance
# then they collided
def isCollision(enemyX, enemyY, bulletX, bulletY, collision_distance):
    distance = math.sqrt(math.pow(enemyX - bulletX, 2) + math.pow(enemyY - bulletY, 2))
    if distance < collision_distance:
        return True
    else:
        return False


# Game loop
running = True
clock = pygame.time.Clock()
while running:

    # Background
    screen.fill(GREEN)
    pygame.draw.rect(screen, BLUE, (200, 0, HEIGHT - 200, WIDTH))
    level_up()
    file = open("scores.txt")
    highscore_value = file.read()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        # if keystroke is pressed check whether is left or right
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                playerX_change = - player_speed
            elif event.key == pygame.K_RETURN:
                if player_is_dead:
                    player_is_dead = False
                    score_value = 0
            elif event.key == pygame.K_RIGHT:
                playerX_change = player_speed
            elif event.key == pygame.K_SPACE:
                if bullet_state is "ready" and not player_is_dead:
                    bulletX = playerX
                    fire_bullet(bulletX, bulletY)
                    bullet_sound = mixer.Sound('laser.wav')
                    bullet_sound.play()
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT and playerX_change < 0:
                playerX_change = 0
            elif event.key == pygame.K_RIGHT and playerX_change > 0:
                playerX_change = 0

    if not player_is_dead:
        # Player Movement
        playerX += playerX_change

        # boundaries check
        if playerX <= 200:
            playerX = 200
        if playerX >= 736:
            playerX = 736

    # Collision for bullet check
    if collision_check_for_bullet(enemy_list) or collision_plane_check_for_bullet(
            enemy_plane_list) or collision_plane_ltr_check_for_bullet(enemy_plane_ltr_list):
        explosion_sound = mixer.Sound('explosion.wav')
        explosion_sound.play()
        bulletY = 600
        bullet_state = "ready"
        score_value += 1

    # display the enemy and update their positions
    if not player_is_dead:
        drop_enemies(enemy_list)
        drop_plane_enemies(enemy_plane_list)
        drop_plane_ltr_enemies(enemy_plane_ltr_list)
        drop_trees(tree_list)
        update_enemy_positions(enemy_list)
        update_enemy_plane_positions(enemy_plane_list)
        update_enemy_plane_ltr_positions(enemy_plane_ltr_list)
        update_tree_positions(tree_list)

        # Game over
        if collision_check(enemy_list) or collision_plane_check(enemy_plane_list) or collision_plane_ltr_check(
                enemy_plane_ltr_list):
            if player_lives > 1:
                game_reset()
            else:
                player_is_dead = True

        draw_trees(tree_list)
        draw_enemies(enemy_list)
        draw_plane_enemies(enemy_plane_list)
        draw_plane_ltr_enemies(enemy_plane_ltr_list)

    if not player_is_dead:
        # Bullet Movement
        if bulletY <= 0:
            bulletY = 600
            bullet_state = "ready"

        if bullet_state is "fire":
            fire_bullet(bulletX, bulletY)
            bulletY -= bulletY_change

        # display the player
        player(playerX, playerY)

        # display the score
        show_score(textX, textY)

        # update every frame
    else:
        # update the highs core if needed
        highscore(score_value)
        game_over()
    clock.tick(180)
    pygame.display.update()
